'use strict';

angular.module('eventsApp')
        .constant("baseURL","http://www.gamersseal.com/calendar_dev/dist/")

        .service('eventsFactory', ['$resource', 'baseURL', function($resource,baseURL) {

            this.getEvents = function(){
                return $resource("/beta/api/rest/events",null,  {'query':{method:'GET', isArray:true},'update':{method:'PUT' }});
            };
            this.addEvents = function(){
                return $resource("/beta/api/rest/events/add",null, {'update':{method:'POST'} });
            };
            
        }])
        .service('hosterFactory', ['$resource', 'baseURL', function($resource,baseURL) {

            this.getHoster = function(){
            	var res = $resource("/beta/api/rest/user/:id", null,  {'get':{method:'GET', isArray:true},'update':{method:'PUT' }});
                return res;
            };
            this.getHosterEvents = function(){
                var resEv = $resource("/beta/api/rest/hoster/:id",null,  {'get':{method:'GET', isArray:true},'update':{method:'PUT' }});
            	return resEv;
            };
            
        }])
;