<?php

        // set up the connection variables
        $db_name  = 'testqz';
        $hostname = 'localhost';
        $username = 'root';
        $password = 'dJ7frCjj';

        // connect to the database
        $dbh = new PDO("mysql:host=$hostname;dbname=$db_name", $username, $password);

        $date=$date-3600*24;
        // a query get all the records from the users table
        $sql = 'select s.user, s.OCI, s.OCO, s.CCP, s.PV, us.total_games, s2.total_games as today_games, s.date			
						from #__user_stat_date s 
							join #__users u on u.id=s.user
							join #__user_stat us on us.user=s.user
							left join #__user_stat_date s2  on s2.user=s.user and s2.date='.$date;

        // use prepared statements, even if not strictly required is good practice
        $stmt = $dbh->prepare( $sql );

        // execute the query
        $stmt->execute();

        // fetch the results into an array
        $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

        // convert to json
        $json = json_encode( $result );

        // echo the json string
        echo $json;
        echo $sql;
?>