'use strict';

var FILTER_ALL = 'FILTER_ALL';

angular.module('eventsApp')
    .directive('bindToHeight', function ($window, $timeout) {

        return {
            restrict: 'A',

            link: function (scope, elem, attrs) {

                var attributes = scope.$eval(attrs['bindToHeight']);
                var targetElem = angular.element(document.querySelector(attributes[1]));
                angular.element($window).bind('resize', function(e) {
                    $timeout(function() {
                        //console.log(targetElem.height());
                    }, 10);
                    $timeout(function() {
                        //console.log(targetElem.height());
                    }, 500);
                    $timeout(function() {
                        //console.log(targetElem.height());
                    }, 1000);
                });
                // Watch for changes
                scope.$watch(function () {
                    //console.log(targetElem.height());
                    return targetElem.height();
                },
                function (newValue, oldValue) {
                    if (newValue != oldValue) {

                        elem.css(attributes[0], newValue);
                        //console.log("add nw2"+newValue);
                    }
                });
            }
        };
    })
    .directive('fancybox', function ($compile, $http) {
        return {
            scope: false,
            restrict: 'EA',

            link: function(scope, element, attrs) {
                scope.openFancybox = function (href) {
                    /*if (href == '/beta/calendar_dev/viewEvent.html') {
                        if (attrs.hoster) {
                            scope.curHosterUID = angular.fromJson(attrs.hoster);
                            href = href+"?"+scope.curHosterUID;
                        } else {
                            scope.curHosterUID = scope.chEvent.event.customer_id;
                            href = href+"?"+scope.curHosterUID;
                        }
                    }*/
                    $http.get(href, {cache:false}).then(function(response) {
                        if (response.status == 200) {

                            var template = angular.element(response.data);
                            var compiledTemplate = $compile(template);
                            compiledTemplate(scope);

                            $.fancybox.open({ content: template, type: 'html', maxWidth: "60%", 
                                beforeLoad: function() {
                                    if(scope.contactSwitch == true) {scope.doSwitch()}
                                }, 
                                afterLoad: function() {
                                    $.fancybox.update();
                                }
                            });
                        }
                    });
                };
            }
        };
    })
    .directive('fancybox3', function ($compile, $http) {
        return {
            scope: false,
            restrict: 'EA',

            link: function(scope, element, attrs) {
                /*
                scope.openFancybox3 = function (href) {
                    console.log(element);
                    if (attrs.hoster) {
                        scope.curHosterUID = angular.fromJson(attrs.hoster);
                    } else {
                        //console.log(scope.chEvent);
                        //scope.curHosterUID = href.replace('#viewEvent', '');
                        scope.curHosterUID = scope.chEvent.event.customer_id;
                    }
            
                    scope.initHoster(scope.curHosterUID);
                    //scope.feedback.hoster_email = scope.curHoster.email;
                    console.log("(init hoster fancybox3) check it for email!");
                    console.log(scope.curHoster);

                    var template = angular.element(href);
                    var compiledTemplate = $compile(template);
                    compiledTemplate(scope);
                    $.fancybox.close();
                    $.fancybox.open({ href: href, type: 'inline', width:"60%",
                        beforeLoad: function() {
                            console.log('fancyStart')
                        }
                    });
                };
                */
                scope.openFancybox3 = function (href) {
                    //href = '/beta/calendar_dev/viewEvent.html';
                    //scope.curHosterUID = angular.fromJson(attrs.hoster);
                    scope.switched = false;
                    var date = angular.fromJson(attrs.event);
                    scope.chEvent =  {event: date};
                    scope.curHosterUID = scope.chEvent.event.customer_id
                    scope.initHoster(scope.curHosterUID);

                    if (href == '/beta/calendar_dev/editEvent.html') {
                        //scope.eventToEdit = scope.chEvent;
                        console.log('EventToEdit: ');
                        //console.log(scope.eventToEdit);
                        scope.switchToConfirm = function(type) {
                            //console.log(type);
                            if (type == 'Del') {
                                scope.switched = true;
                            } 
                            else {
                                scope.switched = false;
                            }
                            $.fancybox.update();
                        };
                        scope.sentEdit = false;
                        scope.editEvent = function(type) {

                            //$scope.eventToEdit = $scope.chEvent.event;

                            //console.log($scope.file.name);
                            if (scope.chEvent.event.image != '') {
                                scope.upload(scope.chEvent.event.file);
                                scope.chEvent.event.file = '';
                            }
                            scope.sentEdit = true;
                            //$scope.newEvent.image = $scope.file.name;

                            scope.chEvent.event.action = type;
                            scope.chEvent.event.enable = "Hide";
                            scope.chEvent.event.updatd = 1;
                            // // $scope.newEvent.customer_id = $scope.curUID;
                            // // $scope.sentType = type;
                            
                            console.log(scope.chEvent.event);
                            
                            $http({
                              url: 'http://gamersseal.com/beta/all-events/',
                              method: "POST",
                              data: $.param(scope.chEvent.event),
                              headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                              }
                            })
                            .success(function(data){
                                    console.log(data);
                                    $.fancybox.close();
                                    $.fancybox.open({ href: '#eventSuccess', type: 'inline', width:"60%" });
                                    //scope.init(scope.curUID);
                            });

                        };                        
                    }
                    //console.log(scope.chEvent.event);
                    //console.log(date);
                    href = href+"?"+scope.curHosterUID;

                    $http.get(href, {cache:false}).then(function(response) {
                        if (response.status == 200) {

                            var template = angular.element(response.data);
                            var compiledTemplate = $compile(template);
                            compiledTemplate(scope);

                            $.fancybox.open({ content: template, type: 'html', maxWidth:"60%",
                                beforeLoad: function() {if(scope.contactSwitch == true) {scope.doSwitch()} }
                            });
                        }
                    });
                };

            }
        };
    })
    .controller('EventsController', ['$scope', '$resource', 'eventsFactory', 'hosterFactory', 'uiCalendarConfig', '$compile', '$timeout', '$filter', '$http', '$window', 'Upload', function($scope, $resource, eventsFactory, hosterFactory, uiCalendarConfig, $compile, $timeout, $filter, $http, $window, Upload) {
        $scope.curUID = 0;
        $scope.curHosterUID = 0;

        $scope.contactSwitch = false;
        $scope.doSwitch = function() {
            $scope.contactSwitch = !$scope.contactSwitch;
            $.fancybox.update();
        };
        $scope.donateLink = '/beta/donate-support/';
        $scope.sentAdd = false;
        $scope.sentType = false;
        $scope.isType = function(type) {
            if (type == $scope.sentType) {
                return true
            }
        };
        /* Sort */
        $scope.userSearch = '';         // search
        $scope.eventTypeSort = 'all';   // default eventType
        $scope.stateSort = '';          // default state
        $scope.userBar = true;
        /* SLICK */
        $scope.numberLoaded = true;
        $scope.slickConfig = {
            enabled: false,
            autoplay: true,
            draggable: true,
            autoplaySpeed: 3000,
            method: {},
            event: {
                init: function (event, slick, currentSlide, nextSlide, $window) {
                    var w = angular.element($window);
                    $timeout(function(){ w.triggerHandler('resize') },10);
                },
                beforeChange: function (event, slick, currentSlide, nextSlide, $window) {
                    var w = angular.element($window);
                    $timeout(function(){ w.triggerHandler('resize') });
                }
            }
        };
        /* User */
        $scope.userShift = function() {
            $scope.userBar = !$scope.userBar;
            $scope.slickConfig.method.slickNext();
            var w = angular.element($window);
            $timeout(function(){ w.triggerHandler('resize') });
        };

        /* CALENDAR */
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $scope.changeTo = 'Hungarian';
        /* event source that pulls from google.com */
        /*$scope.eventSource = {
                url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
                className: 'gcal-event',           // an option!
                currentTimezone: 'America/Chicago' // an option!
        };*/

        // change Magento Array to appropriate data types
        $scope.changeData = function(data){
            //var title;
            for(var i = 0; i < data.length; i++){
                if(data[i].hasOwnProperty("event_day")){
                    //data[i]["date"] = data[i]["event_day"];
                    data[i]["date"] = data[i]["event_day"]+"T"+data[i]["start_time"]+":00.502Z";
                    //delete data[i]["event_day"];
                }
                if(data[i].hasOwnProperty("admission")){
                    data[i]["type"] = data[i]["admission"];
                    delete data[i]["admission"];
                }
                if(data[i].hasOwnProperty("start_time")){
                    if (data[i]["start_time"].length == 5) {
                        data[i]["start_time"] = data[i]["event_day"]+"T"+data[i]["start_time"]+":00.502Z";
                    }
                    data[i]["estart"] = data[i]["start_time"];
                    delete data[i]["start_time"];
                }
                if(data[i].hasOwnProperty("end_time")){
                    if (data[i]["end_time"].length == 5) {
                        data[i]["end_time"] = data[i]["event_day"]+"T"+data[i]["end_time"]+":00.502Z";
                        //data[i]["end_time"] = data[i]["end_time"]+":00";
                    }
                    data[i]["eend"] = data[i]["end_time"];
                    delete data[i]["end_time"];
                }
                if(data[i].hasOwnProperty("address")){
                    data[i]["loc"] = data[i]["address"];
                    delete data[i]["address"];
                }
                if(data[i].hasOwnProperty("ticket_cost")){
                    data[i]["cost"] = data[i]["ticket_cost"];
                    delete data[i]["ticket_cost"];
                }
                if(data[i].hasOwnProperty("point_contact")){
                    data[i]["contact"] = data[i]["point_contact"];
                    delete data[i]["point_contact"];
                }
                if(data[i].hasOwnProperty("dress_code")){
                    data[i]["dress"] = data[i]["dress_code"];
                    delete data[i]["dress_code"];
                }
                if(data[i].hasOwnProperty("purpose_description")){
                    data[i]["desc"] = data[i]["purpose_description"];
                    delete data[i]["purpose_description"];
                }
                if(data[i].hasOwnProperty("slick")){
                    data[i]["slick"] = true;
                }
                // for email to hoster
                if(data[i].hasOwnProperty("email")){
                    $scope.feedback.hoster_email = data[i]["email"];;
                    //console.log("Hoster email: "+$scope.feedback.hoster_email);
                }
                /*
                if(data[i].hasOwnProperty("title")){ //added missing closing parenthesis
                    title = data[i].title;
                    data[i].title = '<span class="red">' + title + '</span>';
                }*/
            }
        }


        // getHoster retrieved hoster info
        $scope.initHoster = function(uid) {
            uid = parseInt(uid, 10);
            $scope.curHosterUID = uid;
            $scope.curHoster = hosterFactory.getHoster().get({id:uid, isArray:true}).$promise.then(
                function(response){
                    respose = $scope.changeData(response);
                    $scope.curHoster = response;
                    //console.log("curHoster: (curHosterUID) "+$scope.curHosterUID);
                    console.log("init Hoster success: ");
                    console.log(response);
                },
                function(response) {
                    console.log("error curHoster:");
                    console.log(response);
                    $scope.message = "Error: "+response.status + " " + response.statusText;
                }
            );
        };

        $scope.init = function(uid) {
            uid = parseInt(uid, 10);
            $scope.curUID = uid;
            $scope.curUser = hosterFactory.getHoster().get({id:uid, isArray:true}).$promise.then(
                function(response){
                    respose = $scope.changeData(response);
                    $scope.curUser = response; 
                    //console.log("curUser: (curUID): "+parseInt($scope.curUID, 10));
                    console.log(response);
                },
                function(response) {
                    console.log("error curUser: ");
                    console.log(response);
                    $scope.message = "Error: "+response.status + " " + response.statusText;
                }
            );
            // getHoster retrieved events info
            $scope.curUserEvents = hosterFactory.getHosterEvents().get({id:uid, isArray:true}).$promise.then(
                function(response){
                    respose = $scope.changeData(response);
                    $scope.curUserEvents = response;
                    console.log("curUserEvents: ");
                    console.log(response);
                },
                function(response) {
                    console.log("error curUserEvents:");
                    console.log(response);
                    $scope.message = "Error: "+response.status + " " + response.statusText;
                }
            );
        }

        $scope.events = eventsFactory.getEvents().query(
                function(response, $filter) {
                    respose = $scope.changeData(response);
                    $scope.events = response;
                    console.log("events: ");
                    console.log(response);
                        $scope.slickConfig = {
                            enabled: true,
                        }
                },
                function(response) {
                    console.log("error events:");
                    console.log(response);
                    $scope.message = "Error: "+response.status + " " + response.statusText;
        });
        /* alert on eventClick */
        $scope.alertOnEventClick = function( date, jsEvent, view){
            //alert(date.title + ' was clicked ');
            //console.dir(date);
            $scope.curHoster = '';
            $scope.chEvent = {event: date};
            $scope.curHosterUID = $scope.chEvent.event.customer_id;
            $scope.initHoster($scope.curHosterUID);
            console.log("init hoster via alertOnEventClick New hoster: "+$scope.curHosterUID);
            if ($scope.curView=='month') {
                console.log(date);
                $scope.gotoDate(date.date);
            }
        };

        /* add and removes an event source of choice */
        $scope.addRemoveEventSource = function(sources,source) {
          var canAdd = 0;
          angular.forEach(sources,function(value, key){
            if(sources[key] === source){
              sources.splice(key,1);
              canAdd = 1;
            }
          });
          if(canAdd === 0){
            sources.push(source);
          }
        };
        /* remove event */
        $scope.remove = function(index) {
          $scope.events.splice(index,1);
        };
        /* Change View */
        $scope.changeView = function(view,calendar) {
          uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
        };
        /* Change View */
        $scope.renderCalender = function(calendar) {
          $timeout(function() {
            if(uiCalendarConfig.calendars[calendar]){
              uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
          });
        };
         /* Render Tooltip */
        $scope.eventRender = function( event, element, view ) {

            if($scope.curView=='day') {
            element.attr("fancybox","").attr("ng-click","openFancybox('/beta/calendar_dev/viewEvent.html')");
                var border = '';
                if (event.type == 'Main') {
                    border = 'border_purple'
                }
                else if (event.type == 'Paid') {
                    border = 'border_cyan' 
                }
                else if (event.type == 'Free') {
                    border = 'border_green'
                };
                element.addClass(border).addClass('eve_user_event');

                var html = '<div class="col-xs-4 eve_nopad eve_bbox">';
                
                if(event.image != '') {
                    html+= '<img ng-src="/beta/media/customer/'+event.image+'" class="img-responsive">';
                }
                    html+= '</div>';

                    html+= '<div class="col-xs-8 eve_nopad-r eve_bbox text-left text-uppercase">';
                        html+= '<div class="event_name">';
                        html+= '    <span class="eve_title">EVENT:</span>';
                        html+= '    <span>'+event.title+', '+event.state+'</span>';
                        html+= '</div>';
                        html+= '<div class="event_date">';
                        html+= '    <span class="eve_title">EVENT DATE:</span>';
                        html+= '    <span>'+$filter('date')(event.date, 'dd MMMM yyyy')+'</span>';
                        html+= '</div>';
                        html+= '<div class="event_time">';
                        html+= '    <span class="eve_title">START TIME:</span>';
                        html+= '    <span>'+$filter('date')(event.estart, 'shortTime');
                        html+= ' - '+$filter('date')(event.eend, 'shortTime')+'</span>';
                        html+= '</div>';
                        html+= '<div class="event_type">';
                        html+= '    <span class="eve_title">EVENT TYPE:</span>';
                        html+= '    <span>'+event.type+'</span>';
                        html+= '</div>';
                    html+= '</div>';
                    html+= '<div class="eve_event_promo text-right">';
                    html+= '    <img ng-src="http://gamersseal.com/beta/skin/frontend/rwd/default/images/media/logo.png" style="height:20px;display:inline"><br>';
                    html+= '    TOP 10<br>PREMIUM';
                    html+= '</div>';
                    html+= '<div class="clearfix"></div>';
                    
                    $('.eve_app').removeClass("month_view");

                element.append(html);
            }
            if($scope.curView=='month') {
                if (event.type == 'Paid' || event.type == 'Main') {
                    var nd = $filter('date')(event.date, 'yyyy-MM-dd');
                    element.addClass("eve_paid_event").append('<style>body div.fc-bg table tr td[data-date="'+nd+'"]{background-image:url(/beta/media/customer/'+event.image+') !important;background-size:cover !important}</style>');
                    $('.eve_app').addClass("month_view");
                }
            }
            element.attr({'tooltip': event.title,
                          'tooltip-append-to-body': true});
            $compile(element)($scope);
            $scope.UpdateSelectedDate();
            var w = angular.element($window);
            $timeout(function(){ w.triggerHandler('resize') });
        };

        $scope.today = new Date(y,m,d);
        $scope.todayMonth = new Date(y,m,1);
        $scope.curView = 'month';
        $scope.expView = false;

        $scope.SelectedDate = $scope.today;

        $scope.UpdateSelectedDate = function () {
            if (uiCalendarConfig.calendars.myCalendar1) {
                $scope.SelectedDate = uiCalendarConfig.calendars.myCalendar1.fullCalendar('getDate')._d;
                }

            var selectedMonth = $scope.SelectedDate.getMonth();
            var selectedYear = $scope.SelectedDate.getFullYear();

            var minDate = new Date(selectedYear, selectedMonth, 1);
            var maxDate = new Date(selectedYear, selectedMonth + 1, 0, 23, 59, 59);

            $scope.SelectedMonthEvents = $scope.events.filter(function (e) {
                var eventDate = new Date(e.date);

                return eventDate >= minDate && eventDate <= maxDate;
            });

            $scope.CheckExpiered();
        };

        $scope.CheckExpiered = function () {
            if ($scope.curView === 'month') {
                var currentMonth = $filter('date')($scope.today, 'MMMM-yyyy');
                var selectedMonth = $filter('date')($scope.SelectedDate, 'MMMM-yyyy');

                if ($scope.today > $scope.SelectedDate && currentMonth !== selectedMonth) {
                    $scope.expView = true;
                } else {
                    $scope.expView = false;
                }
            } else {
                if ($scope.today > $scope.SelectedDate) {
                    $scope.expView = true;
                } else {
                    $scope.expView = false;
                }
            }
        };

        $scope.UpdateSelectedDate();

        $scope.changeViewDay = function() {
            $scope.curView = 'day';
            uiCalendarConfig.calendars['myCalendar1'].fullCalendar( 'changeView', 'basicDay' );
            $scope.UpdateSelectedDate();
        };

        $scope.changeViewMonth = function() {
            $scope.curView = 'month';
            uiCalendarConfig.calendars['myCalendar1'].fullCalendar( 'changeView', 'month' );
            $scope.UpdateSelectedDate();
        };

        $scope.goNext = function() {
            uiCalendarConfig.calendars['myCalendar1'].fullCalendar('next');
            $scope.UpdateSelectedDate();
        };

        $scope.goPrev = function() {
            uiCalendarConfig.calendars['myCalendar1'].fullCalendar('prev');
            $scope.UpdateSelectedDate();
        };

        $scope.gotoDate = function(date) {
            $scope.changeViewDay();
            uiCalendarConfig.calendars['myCalendar1'].fullCalendar( 'gotoDate', date );
            $scope.UpdateSelectedDate();
        };

        $scope.dayClick = function(date, jsEvent, view) {
            $scope.gotoDate(date);
        };

        /* config object */
        $scope.uiConfig = {
            calendar:{
                height: 450,
                editable: false,
                header:{
                    left: '', // title
                    center: '',
                    right: '' // today prev,next
                },
                dayClick: $scope.dayClick,
                eventClick: $scope.alertOnEventClick,
                /*eventDrop: $scope.alertOnDrop,
                eventResize: $scope.alertOnResize,*/
                eventRender: $scope.eventRender,
                firstDay: 1
            }
        };

        $scope.changeLang = function() {
          if($scope.changeTo === 'Hungarian'){
            $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
            $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
            $scope.changeTo= 'English';
          } else {
            $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
            $scope.changeTo = 'Hungarian';
          }
        };
        /* event sources array*/
        $scope.eventSources = [$scope.events]; // $scope.eventsF,$scope.eventSource, $scope.eventsF
        //$scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];

        $scope.FILTER_ALL = FILTER_ALL; // Чтобы можно было использовать в шаблонах разметки

        $scope.Filters = {
            type: $scope.FILTER_ALL,
            state: $scope.FILTER_ALL
        };

        $scope.FiltersChanged = function() {
            var result = $scope.events;

            if ($scope.Filters.type !== FILTER_ALL) {
                result = $filter('filterBy')(result, ['type'], $scope.Filters.type);
            }

            if ($scope.Filters.state !== FILTER_ALL) {
                result = $filter('filterBy')(result, ['state'], $scope.Filters.state);
            }

            $scope.sortedEvents = result;

            uiCalendarConfig.calendars.myCalendar1.fullCalendar('removeEvents');
            uiCalendarConfig.calendars.myCalendar1.fullCalendar('addEventSource', $scope.sortedEvents);
        };
        // upload on file select or drop
        $scope.file = '';
        $scope.upload = function (file) {
            Upload.upload({
                url: '/beta/upload.php',
                data: {file: file, 'curUID': $scope.curUID}
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

        $scope.cleanEvent = {
            title:"",
            event_day:"",
            start_time:"",
            end_time:"",
            address:"",
            state:"",
            ticket_cost:"",
            age:"",
            dress_code:"",
            point_contact:"",
            purpose_description:"",
            admission:"",
            enable:"0",
            image:"",
            file:"",
            stick:"1",
            customer_id:$scope.curUID
        };

    
        $scope.newEvent = $scope.cleanEvent;
        $scope.addEvent = function(type) {
            if (!$scope.sentAdd) {
                //console.log($scope.file.name);
                if ($scope.newEvent.image != '') {
                    $scope.upload($scope.newEvent.file);
                    $scope.newEvent.file = '';
                }
                $scope.sentAdd = true;
                //$scope.newEvent.image = $scope.file.name;

                $scope.newEvent.admission = type;
                $scope.newEvent.customer_id = $scope.curUID;
                $scope.sentType = type;
                
            console.log($scope.newEvent);

            $http({
              url: 'http://gamersseal.com/beta/product/index/addEvent',
              method: "POST",
              data: $.param($scope.newEvent),
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
              }
            })
            .success(function(data){
                    //console.log(data);
                    $.fancybox.close();
                    $.fancybox.open({ href: '#eventSuccess', type: 'inline', width:"60%" });

                        if (type == 'Paid') {
                            $http({
                              url: 'http://gamersseal.com/beta/all-events/',
                              method: "POST",
                              data: $.param($scope.newEvent),
                              headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                              }
                            })
                            .success(function(data){
                                    $.get('http://gamersseal.com/beta/all-events/', function(data) {
                                        data = $(data);
                                        $("#cart").html($('#cart', data).html());
                                    });
                            });
                        };
                    
                    $scope.newEvent = $scope.cleanEvent;
                    //$scope.addEventForm.$setPristine();
                    ///$scope.sentAdd = true;

                    // update cart:
                    //if (type == 'Paid') {
                    /*    $.get('http://gamersseal.com/beta/all-events/', function(data) {
                            data = $(data);
                            $("#cart").html($('#cart', data).html());
                        });
                        */
                    //}
            });
            } 
        };
        
        $scope.cleanFeedback = {contact_fullname:"", contact_email:"", contact_enquiry:"", hoster_email:""};
        $scope.feedback = $scope.cleanFeedback;

        $scope.sendEnquiry = function() {
            console.log($scope.feedback);
            $.fancybox.close();
            $.fancybox.open({ href: '#contactSuccess', type: 'inline', width:"60%" });
            $http({
              url: 'http://gamersseal.com/beta/all-events/',
              method: "POST",
              data: $.param($scope.feedback),
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
              }
            })
            .success(function(data){
                    //console.log(data);
                    //$scope.feedbackForm.$setPristine();
                    $scope.feedback = $scope.cleanFeedback;
            });
        };
    }])
    .filter('filterByState', function () {
        return function (eventList, state) {
            if (state === FILTER_ALL) {
                return eventList;
            }

            return eventList.filter(function (event) {
                return event.state === state;
            });
        };
    });
