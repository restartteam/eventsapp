'use strict';

angular.module('eventsApp', ['ui.router','ngResource', 'angular.filter', 'ui.calendar', 'slickCarousel', 'perfect_scrollbar', 'ngFileUpload', 'moment-picker'])

    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                //url:'/index.php/rating',
                views: {
                    /*'header': {
                        templateUrl : 'views/header.html'
                    },*/
                    'content': {
                        //templateUrl : 'views/home.html',
                        controller  : 'EventsController'
                    }/*,
                    'footer': {
                        templateUrl : 'views/footer.html'
                    }*/
                }
            })
            //$urlRouterProvider.otherwise('/');
    })
    .config(['momentPickerProvider', function (momentPickerProvider) {

    }]);